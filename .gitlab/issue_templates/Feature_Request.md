Summary:

(Brief summary of your new feature)

Feature Details:

(Please provide a brief but informative description of the feature)

(What does it do and how does it function?)

Where would this fit into the Website?:

(Is there a certain section/page?)

Benefit:

(Briefly outline what benefit this feature would bring and to whom)

Urgency:

(Is this feature time sensitive?)

(Eg, An AGM feature should be ready by AGM)

\label ~Feature